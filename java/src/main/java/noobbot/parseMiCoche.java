package noobbot;

public class parseMiCoche {
    public final String msgType;
    public final miCoche data;
    
	public parseMiCoche(String msgType, miCoche data) {
		this.msgType = msgType;
		this.data = data;
	}
}

class miCoche{
	public final String name, color;
	
	public miCoche(String name, String color){
		this.name = name;
		this.color = color;
	}
}