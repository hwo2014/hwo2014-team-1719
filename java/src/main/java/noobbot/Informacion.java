package noobbot;

public class Informacion {
	//TRUBO
	public boolean turbo = false;
	public double turboTicks = 0, turboFactor = 0;
	
	//MI COCHE
	public String miNombre = "ColdBeer";
	public double Peso = 0;	
	
	//GAME INIT
	public int vueltas = 0;
	public boolean tipoCarrera = false;

	public double [] rectas;
	public double [] anguloCurva;
	public double [] radioCurva;
	public boolean [] cambioCarril;
	public double [] centroCarril;
		
	public double Rozamiento = 0;

	public double maxCurva(int PiezaActual) {
		
		int i = PiezaActual+1;
		boolean curva = false;
		
		while(!curva){
			if(rectas[i%rectas.length]==0) curva=true;
			else i++;
		}
		
		double R = radioCurva[i%rectas.length];
		
		return Math.sqrt(Rozamiento*9.8*Peso*(R));
	}
	
}
