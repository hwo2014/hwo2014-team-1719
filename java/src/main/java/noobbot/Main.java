package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import com.google.gson.Gson;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
   
           }

    final Gson gson = new Gson();
    private PrintWriter writer;
    public Informacion INFO = new Informacion();

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
             
             
        send(join);
        
        nuevoBot myBot = new nuevoBot("ColdBeer", "O8q+tyPJM0cXYw");
        send(new CreateRace(myBot, "usa", "pedo", 2));
        send(new JoinRace(myBot, "france", 1));
        
        double Vel1 = 0, Vel2 = 0, RecAux = 0;
        boolean calcRoz = true, Acelero = true;
        
        boolean proxCurva = false;
        double maxCurva = 0;
        
        double RecAnterior = 0, RecActual = 0;
        
        while((line = reader.readLine()) != null) {
        	
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            
 //           System.out.println(line);
            
            if (msgFromServer.msgType.equals("carPositions")) {
            	
            	send(new Throttle(0.5));
            	
//            	System.out.println("carPositions");
            	final parsePosicion Posicion = gson.fromJson(line, parsePosicion.class);
            	
            	int posCarrera = 0;
            	while(!Posicion.data.get(posCarrera).id.name.equals(INFO.miNombre)) posCarrera++;
            	
            	int VueltaActual = Posicion.data.get(posCarrera).piecePosition.lap;
            	int CarrilActual = Posicion.data.get(posCarrera).piecePosition.lane.startLaneIndex;
            	int TickActual = Posicion.gameTick;
            	int PiezaActual = Posicion.data.get(posCarrera).piecePosition.pieceIndex;
            	double RecorridoActual = Posicion.data.get(posCarrera).piecePosition.inPieceDistance;
            	
//            	EN VUELTAS DE CLASIFICACION
            	if(INFO.tipoCarrera){	
           		
            		if(calcRoz){
            			if(Acelero){
            				send(new Throttle(1.0));
            				Vel1 = RecorridoActual;
            				RecAux = RecorridoActual;
            				Acelero = false;
            			} else{
            				send(new Throttle(0.0));
            				Vel2 = RecorridoActual - RecAux;
            				INFO.Rozamiento = (Vel2-Vel1)/2;
            				calcRoz = false;
            			}
            		}
            		else{
//            			System.out.println(INFO.Rozamiento);
            			
            			
            			
//            			ES UNA RECTA
            			if(INFO.rectas[PiezaActual] > 0){
            				
            				if(!proxCurva){
            					maxCurva = INFO.maxCurva(PiezaActual) - 4;
            					proxCurva = true;
//            					System.out.println(maxCurva);
            				}

            				
            				double distFreno = 0;
            				double velActual = RecorridoActual-RecAnterior;
            				
            				if(velActual>30)
            					distFreno = ((maxCurva*maxCurva)-(velActual*velActual)) / (2*INFO.Rozamiento);
            				
//            				System.out.println(velActual);
//            				System.out.println(distFreno);
            				
            				if(INFO.rectas[PiezaActual] > distFreno+10) send(new Throttle(1.0));
            				else{
            					send(new Throttle(0.0));
            				}
            				
            			} 
//            			ES UNA CURVA
            			else{
            				send(new Throttle(0.5));
            				proxCurva = false;
            			}
            			
            			RecAnterior = RecorridoActual;
            			
            		}
            		
            	} 
//            	EN CARRERA
            	else{
            		            		
            	}
            	
            } else if (msgFromServer.msgType.equals("join")) {
               
            	System.out.println("Joined");

            	final parseJoin Join = gson.fromJson(line, parseJoin.class);
         	                
            	
            } else if (msgFromServer.msgType.equals("yourCar")) {
            	
//            	System.out.println("yourCar");

            	final parseMiCoche myCar = gson.fromJson(line, parseMiCoche.class);
            	
            	
            } else if (msgFromServer.msgType.equals("gameInit")) {

            	System.out.println("gameInit");
                System.out.println("Inicio Alfonso");
            	final parseGameInit inicio = gson.fromJson(line, parseGameInit.class);
            	

            	int pos = 0;
            	while(!inicio.data.race.cars.get(pos).id.name.equals(INFO.miNombre)) pos++;
            	
            	INFO.Peso = inicio.data.race.cars.get(pos).dimensions.width;
                       	
            	INFO.vueltas = inicio.data.race.raceSession.laps;
            	INFO.tipoCarrera = inicio.data.race.raceSession.quickRace;
            	
            	ArrayList<Piezas> PIEZAS = inicio.data.race.track.pieces;
            	INFO.rectas = new double[PIEZAS.size()];
            	INFO.anguloCurva = new double[PIEZAS.size()];
            	INFO.radioCurva = new double[PIEZAS.size()];
            	INFO.cambioCarril = new boolean[PIEZAS.size()];
            	
            	for (int i = 0; i < PIEZAS.size(); i++) {
//            		CARRILES
            		INFO.cambioCarril[i] = PIEZAS.get(i).isSwitch;
            		
//            		RECTAS
            		double dist = PIEZAS.get(i).length;
            		
            		if(dist>0){
            			
	            		int k = 1;
						while (PIEZAS.get((i+k)%PIEZAS.size()).length>0) {
							dist += PIEZAS.get((i+k)%PIEZAS.size()).length;
							k++;
						}
						
						INFO.rectas[i] = dist;
//					CURVAS
            		} else{
            			INFO.anguloCurva[i] = PIEZAS.get(i).angle;
            			INFO.radioCurva[i] = PIEZAS.get(i).radius;
            		}
            		
//					System.out.println("Pieza " + i + ": " + INFO.rectas[i] + " - " + INFO.anguloCurva[i] + " - " + INFO.radioCurva[i] + " - " + INFO.cambioCarril[i]);
				}

            	ArrayList<Carriles> CARRILES = inicio.data.race.track.lanes;
            	INFO.centroCarril = new double[CARRILES.size()];
            	for (int i = 0; i < CARRILES.size(); i++) {
					INFO.centroCarril[i] = CARRILES.get(i).distanceFromCenter;
				}
            	
            	
                
            } else if (msgFromServer.msgType.equals("gameEnd")) {
            	
                System.out.println("Race end");
                
            	final parseGameEnd fin = gson.fromJson(line, parseGameEnd.class);
                
                
            } else if (msgFromServer.msgType.equals("gameStart")) {
            	
//                System.out.println("game start");
                
            	final parseRaceStart start = gson.fromJson(line, parseRaceStart.class);
                send(new Throttle(1));
                
            } else if (msgFromServer.msgType.equals("turboAvailable")) {
            	
//                System.out.println("Turbo");
                
            	final parseTurbo turbo = gson.fromJson(line, parseTurbo.class);
            	
            	INFO.turbo = true;
            	INFO.turboFactor = turbo.data.turboFactor;
            	INFO.turboTicks = turbo.data.turboDurationTicks;
            
        	} else {
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}


class CreateRace extends SendMsg {   
	public final nuevoBot botId;
	public final String trackName;
	public String password = "";
	public final int carCount;
	
	CreateRace(nuevoBot botId, String trackName, int carCount) {
		this.botId = botId;
		this.trackName = trackName;
		this.carCount = carCount;
	}
	
	CreateRace(nuevoBot botId, String trackName, String password, int carCount) {
		this.botId = botId;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}
	
    @Override
    protected String msgType() {
        return "createRace";
    }
}

class JoinRace extends SendMsg {   
	public final nuevoBot botId;
	public String trackName = "";
	public String password = "";
	public final int carCount;

	JoinRace(nuevoBot botId, int carCount) {
		this.botId = botId;
		this.carCount = carCount;
	}
	
	JoinRace(nuevoBot botId, String trackName, int carCount) {
		this.botId = botId;
		this.trackName = trackName;
		this.carCount = carCount;
	}
	
	JoinRace(nuevoBot botId, String trackName, String password, int carCount) {
		this.botId = botId;
		this.trackName = trackName;
		this.carCount = carCount;
	}
	
    @Override
    protected String msgType() {
        return "joinRace";
    }
}

class nuevoBot{
	public final String name, key;
	
	nuevoBot(String name, String key){
		this.name = name;
		this.key = key;
	}
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class SwitchLane extends SendMsg {
    private String value;

    public SwitchLane(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}

class Turbo extends SendMsg {
    private String value;

    public Turbo(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}