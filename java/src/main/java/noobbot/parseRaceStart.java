package noobbot;


public class parseRaceStart {
    public final String msgType;
    public final Object data;
    public final int gameTick;
    
	public parseRaceStart(String msgType, Object data, int gameTick) {
		this.msgType = msgType;
		this.data = data;
		this.gameTick = gameTick;
	}
}
