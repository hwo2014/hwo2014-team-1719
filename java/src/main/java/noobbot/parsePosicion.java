package noobbot;

import java.util.ArrayList;

public class parsePosicion {
    public final String msgType, gameId;
    public final ArrayList<Posicion> data;
    public final int gameTick;

    parsePosicion(final String tipo, final ArrayList<Posicion> pos, final String gameId, final int tick) {
        this.msgType = tipo;
        this.data = pos;
        this.gameId = gameId;
        this.gameTick = tick;
    }
}

class Posicion {
	public final miId id;
    public final double angle;
    public final miPieza piecePosition;
    
	public Posicion(miId id, double angle, miPieza piecePosition) {
		this.id = id;
		this.angle = angle;
		this.piecePosition = piecePosition;
	}
}

class miId {
	public final String name, color;

	public miId(String name, String color) {
		this.name = name;
		this.color = color;
	}
}

class miPieza{
	public final int pieceIndex, lap;
	public final double inPieceDistance;
	public final Carril lane;
	
	public miPieza(int pieceIndex, double inPieceDistance, Carril lane, int lap) {
		this.pieceIndex = pieceIndex;
		this.inPieceDistance = inPieceDistance;
		this.lane = lane;
		this.lap = lap;
	}
}

class Carril {
	public final int startLaneIndex, endLaneIndex;

	public Carril(int startLaneIndex, int endLaneIndex) {
		this.startLaneIndex = startLaneIndex;
		this.endLaneIndex = endLaneIndex;
	}
	
}



