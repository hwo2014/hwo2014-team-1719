package noobbot;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class parseGameInit {
    public final String msgType;
    public final Circuito data;
    
	public parseGameInit(String msgType, Circuito data) {
		this.msgType = msgType;
		this.data = data;
	}
}


class Circuito{
    public final Carrera race;
	
    public Circuito(Carrera carrera){
    	this.race = carrera;
    }
}


class Carrera{
    public final Pista track;
    public final ArrayList<Coches> cars;
    public final Sesion raceSession;
    
	public Carrera(Pista track, ArrayList<Coches> cars, Sesion raceSession) {
		this.track = track;
		this.cars = cars;
		this.raceSession = raceSession;
	}
}


class Pista{
	public final String id, name;
	public final ArrayList<Piezas> pieces;
	public final ArrayList<Carriles> lanes;
	public final Object startingPoint;
	
	public Pista(String id, String name, ArrayList<Piezas> pieces, ArrayList<Carriles> lanes, Object startingPoint) {
		this.id = id;
		this.name = name;
		this.pieces = pieces;
		this.lanes = lanes;
		this.startingPoint = startingPoint;
	}
}


class Piezas{
	public double length=0, radius=0, angle=0;
	@SerializedName("switch") public boolean isSwitch;
	
	public Piezas(double angle, double radius,  boolean switch1) {
		this.radius = radius;
		this.angle = angle;
		this.isSwitch = switch1;
	}
	

	public Piezas(double length,  boolean switch1) {
		this.length = length;
		this.isSwitch = switch1;
	}
}


class Carriles{
	public final double distanceFromCenter;
	public final int index;
	
	public Carriles(double distanceFromCenter, int index) {
		this.distanceFromCenter = distanceFromCenter;
		this.index = index;
	}
}


class Coches{
    public final idCoche id;
    public final dimension dimensions;
    
	public Coches(idCoche id, dimension dimensions) {
		this.id = id;
		this.dimensions = dimensions;
	}
}

class idCoche{
	public final String name, color;
	
	public idCoche(String name, String color){
		this.name = name;
		this.color = color;
	}
}

class dimension{
	public final double length, width, guideFlagPosition;

	public dimension(double length, double width, double guideFlagPosition) {
		this.length = length;
		this.width = width;
		this.guideFlagPosition = guideFlagPosition;
	}
}

class Sesion{
    public final int laps;
    public final int maxLapTimeMs;
    public final boolean quickRace;
    
	public Sesion(int laps, int maxLapTimeMs, boolean quickRace) {
		this.laps = laps;
		this.maxLapTimeMs = maxLapTimeMs;
		this.quickRace = quickRace;
	}
}