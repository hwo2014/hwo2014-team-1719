package noobbot;

public class parseJoin {
    public final String msgType;
    public final misDatos data;
    
	public parseJoin(String msgType, misDatos data) {
		this.msgType = msgType;
		this.data = data;
	}

}

class misDatos{
	public final String name, key;

	public misDatos(String name, String key) {
		this.name = name;
		this.key = key;
	}
}