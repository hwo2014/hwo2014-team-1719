package noobbot;

public class parseGameEnd{
    public final String msgType;
    public final Acabada data;
    
	public parseGameEnd(String msgType, Acabada data) {
		this.msgType = msgType;
		this.data = data;
	}
}

class Acabada{
    public final Object results;
    public final Object bestLaps;
    
	public Acabada(Object results, Object bestLaps) {
		this.results = results;
		this.bestLaps = bestLaps;
	}
}
