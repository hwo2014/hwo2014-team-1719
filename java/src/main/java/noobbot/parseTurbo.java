package noobbot;

public class parseTurbo {
    public final String msgType;
    public final datosTurbo data;
    
	public parseTurbo(String msgType, datosTurbo data) {
		this.msgType = msgType;
		this.data = data;
	}
}

class datosTurbo{
	public final double turboDurationMilliseconds;
	public final double turboDurationTicks;
	public final double turboFactor;
	
	public datosTurbo(double turboDurationMilliseconds, double turboDurationTicks, double turboFactor) {
		this.turboDurationMilliseconds = turboDurationMilliseconds;
		this.turboDurationTicks = turboDurationTicks;
		this.turboFactor = turboFactor;
	}
}